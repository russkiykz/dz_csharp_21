﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkSerialization
{
    public class Location
    {
        public string Name { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string Lat { get; set; }
        public string Lon { get; set; }
        public string TimezoneId { get; set; }
        public DateTime Localtime { get; set; }
        public long LocaletimeEpoch { get; set; }
        public string UtcOffset { get; set; }
    }
}
