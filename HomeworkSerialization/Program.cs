﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace HomeworkSerialization
{
    class Program
    {
        static void Main(string[] args)
        {
            string json = "";
            using (FileStream stream = File.OpenRead("data.json"))
            {
                byte[] data = new byte[stream.Length];
                stream.Read(data, 0, data.Length);
                json = Encoding.Default.GetString(data);
            }

            Weather weather = JsonConvert.DeserializeObject<Weather>(json);

            Console.WriteLine($"{weather.Location.Country} {weather.Location.Name}\n{weather.Location.Lat},{weather.Location.Lon}\nTemperature:{weather.Current.Temperature}");

        }
    }
}
