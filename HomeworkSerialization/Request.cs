﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HomeworkSerialization
{
    
    public class Request
    {
        public string Type { get; set; }
        public string Query { get; set; }
        public string Language { get; set; }
        public string Unit { get; set; }
    }
}
