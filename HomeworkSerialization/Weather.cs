﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkSerialization
{
    public class Weather
    {
        public Request Request { get; set; }
        public Location Location { get; set; }
        public Current Current { get; set; }
    }
}
